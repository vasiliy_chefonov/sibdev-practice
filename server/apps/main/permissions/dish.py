from rest_framework.permissions import BasePermission, SAFE_METHODS

from apps.eats.models import Restaurant


class IsOwnerOrReadOnlyDish(BasePermission):
    def has_permission(self, request, view):
        if request.method == 'POST':
            return Restaurant.objects.filter(pk=request.data['restaurant'], owner=request.user).exists()
        return True

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.restaurant.owner == request.user
