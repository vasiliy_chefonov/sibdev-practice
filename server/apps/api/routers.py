from rest_framework import routers

from apps.test.viewsets import TestViewSet
from apps.users.viewsets.views import UserViewSet
from apps.eats.viewsets import RestaurantViewSet, DishViewSet, IngredientViewSet


router = routers.DefaultRouter()
router.register('test', TestViewSet, base_name='test')
router.register('users', UserViewSet, base_name='users')
router.register('eats', RestaurantViewSet, base_name='eats')
router.register('dishes', DishViewSet, base_name='dishes')
router.register('ingredients', IngredientViewSet, base_name='ingredients')

