from rest_framework import mixins, viewsets
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.parsers import FormParser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status

from apps.users.serializers.serializers import UserSerializer

from django.contrib.auth.models import User


class UserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    create: Create User instance
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        token, created = Token.objects.get_or_create(user=serializer.instance)
        return Response({'user':serializer.data,'token': token.key}, status=status.HTTP_201_CREATED, headers=headers)
    parser_classes = (FormParser,)


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'user': UserSerializer(user).data,
            'token': token.key,
        })


