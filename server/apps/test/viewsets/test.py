from rest_framework import mixins, viewsets

from apps.test.models import Test
from apps.test.serializers import TestSerializer


class TestViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = TestSerializer
    queryset = Test.objects.all()
