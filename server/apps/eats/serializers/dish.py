from rest_framework.serializers import ModelSerializer

from apps.eats.models import Dish


class DishSerializer(ModelSerializer):
    class Meta:
        model = Dish
        fields = '__all__'
        extra_kwargs = {'total_calories': {'read_only': True}}