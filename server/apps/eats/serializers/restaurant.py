from rest_framework.serializers import ModelSerializer

from apps.eats.models import Restaurant


class RestaurantSerializer(ModelSerializer):
    class Meta:
        model = Restaurant
        exclude = ("owner",)
        extra_kwargs = {'latitude': {'read_only': True}, 'longitude': {'read_only': True},
                        'average_cost': {'read_only': True}}

    def create(self, validated_data):
        validated_data["owner"] = self.context['request'].user
        return super().create(validated_data)
