from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from apps.eats.models import Restaurant
from apps.eats.serializers.restaurant import RestaurantSerializer

from apps.main.permissions.restaurant import IsOwnerOrReadOnlyRestaurant


class RestaurantViewSet(ModelViewSet):
    """
    retrieve: Get a single Restaurant instance

    list: Get a list of all restaurants

    create: Create a Restaurant instance

    destroy: Delete a Restaurant instance

    update: Update a Restaurant instance
    """
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnlyRestaurant)
    serializer_class = RestaurantSerializer
    queryset = Restaurant.objects.all()
    parser_classes = (MultiPartParser,)
