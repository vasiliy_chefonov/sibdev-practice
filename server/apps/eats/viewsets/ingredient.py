from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import GenericViewSet, mixins

from url_filter.integrations.drf import DjangoFilterBackend

from apps.eats.models import Ingredient
from apps.eats.serializers.ingredient import IngredientSerializer


class IngredientViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, GenericViewSet):
    """
    retrieve: Get a single Ingredient instance

    list: Get a list of all ingredients
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = IngredientSerializer
    queryset = Ingredient.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('dish',)

