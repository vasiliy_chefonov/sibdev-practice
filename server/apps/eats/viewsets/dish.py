from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ModelViewSet

from url_filter.integrations.drf import DjangoFilterBackend

from apps.main.permissions.dish import IsOwnerOrReadOnlyDish

from apps.eats.models import Dish
from apps.eats.serializers.dish import DishSerializer


class DishViewSet(ModelViewSet):
    """
    retrieve: Get a single Dish instance

    list: Get a list of all dishes

    create: Create a Dish instance

    destroy: Delete a Dish instance

    update: Update a Dish instance
    """
    permission_classes = (IsAuthenticatedOrReadOnly, IsOwnerOrReadOnlyDish)
    serializer_class = DishSerializer
    queryset = Dish.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ("restaurant",)
    parser_classes = (MultiPartParser,)

