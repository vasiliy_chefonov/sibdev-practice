from django.contrib import admin

from apps.eats.models import Restaurant, Dish, Ingredient


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'photo', 'open_time', 'close_time', 'address', 'owner',
    )
    readonly_fields = ('latitude', 'longitude',)


@admin.register(Dish)
class DishAdmin(admin.ModelAdmin):
    readonly_fields = ('total_calories',)


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    pass
