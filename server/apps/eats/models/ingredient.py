from django.db import models


class Ingredient(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=255
    )
    calorific = models.PositiveIntegerField(
        verbose_name="Калорийность"
    )
    dish = models.ForeignKey(
        "eats.Dish",
        verbose_name="Блюдо",
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Ингредиент'
        verbose_name_plural = 'Ингредиенты'

