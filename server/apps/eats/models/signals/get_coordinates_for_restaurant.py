from django.db.models.signals import pre_save
from django.dispatch import receiver

from yandex_geocoder import Client

from apps.eats.models import Restaurant


@receiver(pre_save, sender=Restaurant)
def get_coordinates(sender, instance, **kwargs):
    if instance.latitude is not None:
        coordinates = Client.coordinates(instance.address)
        instance.latitude = coordinates[0]
        instance.longitude = coordinates[1]
