from django.db.models import Sum
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.eats.models import Dish, Ingredient


@receiver(post_save, sender=Ingredient)
def count_total_calories(sender, instance, **kwargs):
    costs_sum = Ingredient.objects.filter(dish=instance.dish).aggregate(Sum("calorific"))
    Dish.objects.filter(pk=instance.dish.pk).update(total_calories=costs_sum["calorific__sum"])
