from .get_coordinates_for_restaurant import get_coordinates
from .count_total_calories_for_dish import count_total_calories
from .count_average_cost_for_restaurant import count_average_cost
