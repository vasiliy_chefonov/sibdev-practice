from django.db.models import Avg
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.eats.models import Restaurant, Dish


@receiver(post_save, sender=Dish)
def count_average_cost(sender, instance, **kwargs):
    avg = Dish.objects.filter(restaurant=instance.restaurant).aggregate(Avg('cost'))
    Restaurant.objects.filter(pk=instance.restaurant.pk).update(average_cost=avg["cost__avg"])

