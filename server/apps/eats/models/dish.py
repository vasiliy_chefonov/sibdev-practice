from django.db import models


class Dish(models.Model):
    name = models.CharField(
        verbose_name="Название",
        max_length=255
    )
    photo = models.ImageField(
        verbose_name="Фотография"
    )
    total_calories = models.PositiveIntegerField(
        verbose_name="Суммарная калорийность",
        blank=True,
        null=True
    )
    cost = models.PositiveIntegerField(
        verbose_name="Цена"
    )
    restaurant = models.ForeignKey(
        'eats.Restaurant',
        verbose_name="Заведение",
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Блюдо'
        verbose_name_plural = 'Блюда'

