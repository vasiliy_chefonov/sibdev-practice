from django.db import models

from django.contrib.auth.models import User


class Restaurant(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name='Название'
    )
    photo = models.ImageField(
        verbose_name='Фотография'
    )
    open_time = models.TimeField(
        verbose_name='Время открытия'
    )
    close_time = models.TimeField(
        verbose_name='Время закрытия'
    )
    address = models.CharField(
        max_length=500,
        verbose_name="Адрес"
    )
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name="Владелец",
        null=True,
        blank=True
    )
    latitude = models.CharField(
        max_length=255,
        verbose_name="Широта"
    )
    longitude = models.CharField(
        max_length=255,
        verbose_name="Долгота"
    )
    average_cost = models.PositiveIntegerField(
        verbose_name="Средняя стоимость блюд",
        null=True,
        blank=True
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Заведение'
        verbose_name_plural = 'Заведения'

