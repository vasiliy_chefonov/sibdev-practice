from apps.api.routers import router
from apps.users.viewsets.views import CustomAuthToken
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
   openapi.Info(
      title="Restaurants API",
      default_version='v1',
      description="This application is used to search for the nearest restaurants. "
                  "Using the API, it is possible to create new restaurants, attach creators and dishes to them. "
                  "Also, the ingredients are attached to the dishes. "
                  "A token is used as a method of user authentication. "
                  "Only an authorized user can create places and dishes. "
                  "Only the owner of the restaurant can edit place, add and edit dishes. "
                  "All users can view restaurants with their dishes.",
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    # A swagger-ui view of API specification
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api-token-auth/', CustomAuthToken.as_view()),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

    import debug_toolbar

    urlpatterns += [
          path('__debug__/', include(debug_toolbar.urls)),
    ]
