import csv

import urllib.request

from django.db import migrations

from bs4 import BeautifulSoup


BASE_URL = 'http://www.takzdorovo.ru/db/nutritives/'


def get_ingredients(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Ingredient = apps.get_model('eats', 'Ingredient')
    ingredients = []
    response = urllib.request.urlopen(BASE_URL)
    soup = BeautifulSoup(response.read())
    tables = soup.find_all('table', class_='energy-res-tb')
    for table in tables:
        rows = table.find_all('tr')
        for row in rows:
            cols = row.find_all('td')
            ingredients.append({
                'name': row.find('th').text,
                'calories': cols[3].a.text
            })
    ingredient_objects = [Ingredient(name=ingredient['name'], calorific=ingredient['calories']) for ingredient in ingredients]
    Ingredient.objects.bulk_create(ingredient_objects)


class Migration(migrations.Migration):

    dependencies = [
        ('eats', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(get_ingredients),
    ]
